import React from "react";
import TelegramApp from "./TelegramApp";
import "antd/dist/antd.css";
import "./style.css";

function App() {
  return (
    <div className="container">
      <TelegramApp />
    </div>
  );
}

export default App;
