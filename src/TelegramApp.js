import React, { Component } from "react";
import TdClient from "tdweb";

import AuthorizationPhoneForm from "./containers/AuthorizationPhoneForm";
import AuthorizationCodeForm from "./containers/AuthorizationCodeForm";

export class TelegramApp extends Component {
  state = {
    children: null
  };

  componentDidMount() {
    this.initTdClient();
  }

  initTdClient = () => {
    this.client = new TdClient({
      isBackground: true,
      instanceName: "VicionTdLib",
      mode: "wasm",
      useDatabase: false,
      jsLogVerbosityLevel: "info",
      readOnly: false,
      onUpdate: this.handleTdClientUpdates
    });
  };

  handleTdClientUpdates = update => {
    if (update["@type"] === "updateAuthorizationState") {
      if (
        update.authorization_state["@type"] ===
        "authorizationStateWaitTdlibParameters"
      ) {
        this.client.send({
          "@type": "setTdlibParameters",
          parameters: {
            "@type": "tdParameters",
            use_test_dc: false,
            api_id: 814466,
            api_hash: "81b168afe61d06cdc9421abdda3eaa8d",
            system_language_code: "en",
            use_secret_chats: true,
            use_message_database: true,
            use_file_database: false,
            database_directory: "/db",
            device_model: this.getBrowser(),
            system_version: this.getOSName(),
            application_version: "0.1.0"
          }
        });
      } else if (
        update.authorization_state["@type"] ===
        "authorizationStateWaitEncryptionKey"
      ) {
        this.client.send({
          "@type": "checkDatabaseEncryptionKey",
          "@extra": {
            encryption_key: "VicionTdLib_KEY"
          }
        });
      } else if (
        update.authorization_state["@type"] ===
        "authorizationStateWaitPhoneNumber"
      ) {
        let initialValues = {
          phoneNumber: "+84363855778"
        };

        this.setState({
          children: (
            <AuthorizationPhoneForm
              onSubmit={this.onSubmitPhoneForm}
              initialValues={initialValues}
            />
          )
        });
      } else if (
        update.authorization_state["@type"] === "authorizationStateWaitCode"
      ) {
        let initialValues = {
          code: ""
        };

        this.setState({
          children: (
            <AuthorizationCodeForm
              onSubmit={this.onSubmitConfirmCode}
              initialValues={initialValues}
            />
          )
        });
      } else if (
        update.authorization_state["@type"] === "authorizationStateReady"
      ) {
        this.client.send({
          "@type": "getMe"
        }).then(response =>{
          console.log(response);
        })
        .catch(error => {
          console.log(error);
        });
      } else {
        console.log(update);
      }
    }
  };

  getBrowser = () => {
    let browser_name = "";
    let isIE = /*@cc_on!@*/ false || !!document.documentMode;
    let isEdge = !isIE && !!window.StyleMedia;
    if (navigator.userAgent.indexOf("Chrome") !== -1 && !isEdge) {
      browser_name = "Chrome";
    } else if (navigator.userAgent.indexOf("Safari") !== -1 && !isEdge) {
      browser_name = "Safari";
    } else if (navigator.userAgent.indexOf("Firefox") !== -1) {
      browser_name = "Firefox";
    } else if (
      navigator.userAgent.indexOf("MSIE") !== -1 ||
      !!document.documentMode === true
    ) {
      //IF IE > 10
      browser_name = "IE";
    } else if (isEdge) {
      browser_name = "Edge";
    } else {
      browser_name = "Unknown";
    }

    return browser_name;
  };

  getOSName = () => {
    let OSName = "Unknown";
    if (window.navigator.userAgent.indexOf("Windows NT 10.0") !== -1)
      OSName = "Windows 10";
    if (window.navigator.userAgent.indexOf("Windows NT 6.2") !== -1)
      OSName = "Windows 8";
    if (window.navigator.userAgent.indexOf("Windows NT 6.1") !== -1)
      OSName = "Windows 7";
    if (window.navigator.userAgent.indexOf("Windows NT 6.0") !== -1)
      OSName = "Windows Vista";
    if (window.navigator.userAgent.indexOf("Windows NT 5.1") !== -1)
      OSName = "Windows XP";
    if (window.navigator.userAgent.indexOf("Windows NT 5.0") !== -1)
      OSName = "Windows 2000";
    if (window.navigator.userAgent.indexOf("Mac") !== -1) OSName = "Mac/iOS";
    if (window.navigator.userAgent.indexOf("X11") !== -1) OSName = "UNIX";
    if (window.navigator.userAgent.indexOf("Linux") !== -1) OSName = "Linux";

    return OSName;
  };

  onSubmitPhoneForm = formProps => {
    const { phoneNumber } = formProps;

    this.client.send({
      "@type": "setAuthenticationPhoneNumber",
      phone_number: phoneNumber
    });
  };

  onSubmitConfirmCode = formProps => {
    const { code } = formProps;

    this.client.send({
      "@type": "checkAuthenticationCode",
      code: code
    });
  };

  render() {
    return <div>{this.state.children}</div>;
  }
}

export default TelegramApp;
