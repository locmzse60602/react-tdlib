import React from "react";
import { Form, Field } from "formik";
import { AntInput } from "../CreateAntFields/CreateAntFields";

import { isRequired } from "../ValidateFields/ValidateFields";

export default ({ handleSubmit, values, submitCount }) => {
  return (
    <Form className="form-container" onSubmit={handleSubmit}>
      <Field
        component={AntInput}
        name="phoneNumber"
        label="Phone"
        validate={isRequired}
        defaultValue={values.phoneNumber}
        submitCount={submitCount}
        hasFeedback
      />
      <div className="submit-container">
        <button className="ant-btn ant-btn-primary" type="submit">
          Submit
        </button>
      </div>
    </Form>
  );
};
