import React, { Component } from "react";
import { Formik } from "formik";
import InputConfirmCodeForm from "../../components/InputConfirmCodeForm";

export default class AuthorizationCodeForm extends Component {
  constructor(props) {
    super(props);
    this.props = props;
  }

  render = () => (
    <Formik
      initialValues={this.props.initialValues}
      onSubmit={this.props.onSubmit}
      render={InputConfirmCodeForm}
    />
  );
}
