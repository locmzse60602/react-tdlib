import React, { Component } from "react";
import { Formik } from "formik";
import InputPhoneForm from "../../components/InputPhoneForm";

export default class AuthorizationPhoneForm extends Component {
  constructor(props) {
    super(props);
    this.props = props;
  }

  render = () => (
    <Formik
      initialValues={this.props.initialValues}
      onSubmit={this.props.onSubmit}
      render={InputPhoneForm}
    />
  );
}
